# Notifications.jl


Provides an interface for sending system notifications.
It is a fork of [Notifier.jl](https://github.com/goropikari/Notifier.jl/blob/master/LICENSE.md) package.

The aim was to broaden the functionality of Linux part of the package, by adding more backends for playing sounds and speech synthesis.
The macOS and Windows parts were not tested so far. Yet no major changes were done to them.

## Features

- Linux and macOS
    - popup notification (desktop notification via notify-send from libnotify)
    - sound notification (via aplay, sox or vlc)
    - say notification (read messages aloud via espeak or festival)
    - email notification (via mailutils)
    - count up and count down timer
- Windows (Experimental)
    - popup notification (desktop notification)
    - sound notification
    - say notification (Read a given message aloud)
    - count up and count down timer
    
## Installation

```julia
using Pkg
Pkg.add("https://gitlab.com/adrian-tymorek/notifications.jl.git")
```

## Setup for Linux user
Before using Notifications.jl, you need to install following softwares in your Linux system.

- `libnotify` for a desktop notification
    - Debian-based: `# apt-get install libnotify-bin`
    - Arch: `# pacman -S libnotify`
- `mail` for an e-mail notification
    - Debian-based: `# apt-get install mailutils heirloom-mailx bsd-mailx`
    - Arch: `# pacman -S mailutils`
- sound notification (choose one):
    - `aplay`
        - Debian-based: `# apt-get install alsa-utils`
        - Arch: `# pacman -S alsa-utils`
    - `sox`
        - Debian-based: `# apt-get install sox`
        - Arch: `# pacman -S sox`
    - `vlc`
        - Debian-based: `# apt-get install vlc`
        - Arch: `# pacman -S vlc`
- reading messages aloud (choose one):
    - `festival` (recommended due to subjectively better quality)
        - Debian-based: `# apt-get install festival`
        - Arch: 
            - `# pacman -S festival festival-us`
            - and follow [ArchWiki festival configurations section](https://wiki.archlinux.org/index.php/Festival#Configuration)
    - `espeak`
        - Debian-based: `# apt-get install espeak`
        - Arch: `# pacman -S espeak`



E-mail notifications may require additional configuration of mail server.
If following command works correctly, you don't need further setting.
```bash
$ echo test | mail -s foo yourmail@example.com
```

## Uasage

### popup notification
```julia
using Notifier
notify("Task completed")
# defalut title is "Julia".
# You can change the title by title option.
notify("Task completed", title="foofoo")
notify("Task completed", sound=true) # with sound
notify("Task completed", sound="foo.wav") # Specify a sound file (for Linux and Windows)
```
Linux  
![Screenshot of a Notification](./docs/linuxpopup.png?raw=true)

macOS  
![Screenshot of a Notification](./docs/macpopup.png?raw=true)

Windows  
![Screenshot of a Notification](./docs/winpopup.png?raw=true)

### sound and say notification
```julia
alarm() # only sound. You can specify a sound file, alarm(sound="foo.wav")
say("Finish calculation!") # Read aloud
```


### e-mail notification
```julia
email("message", To="foo@example.com") # default subject is set by date.
email("message", subject="result", To="foo@example.com")
```


If you use `email` function frequently, I recommend you to register your email address by `register_email` function.
```julia
julia> register_email()
Type your desired recipient e-mail address to receive a notification.
e-mail: foo@example.com

Recipient e-mail address is saved at /path/to/.julia/v0.6/Notifier/email/address.txt.
If you want to change the address, modify /path/to/.julia/v0.6/Notifier/email/address.txt directly or run register_email() again
```

After you registered, you don't need to specify e-mail address.
```julia
email("message")
```
