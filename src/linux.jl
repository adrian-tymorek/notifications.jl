include(joinpath("defaults", "defaults.jl"))

"""
---
    Notifications.notify(message; title="Julia", sound=false, time=4, icon)

Notify by desktop notification.

# Arguments
- `urgency::AbstractString` : Urgency level ("low", "normal", "critical"). Default is "normal".
- `sound::Union{Bool, AbstractString}`: Play default sound or specific sound.
- `time::Real`: Display time.
- `icon`: Default is Julia logo
- `app_name::AbstractString` : Name of application sending the notification. Default is the name of your script (e.g. "test.jl") or "Julia REPL"/"Atom Juno" if using any of these two.
- `audio_backend::AbstractString` : a CLI audio program used ("aplay","sox","vlc")
"""
function notify(message::AbstractString;
                title="Julia",
                urgency::AbstractString="normal",
                sound::Union{Bool, AbstractString}=false,
                time::Real=4,
                app_name::AbstractString=PROGRAM_FILE,
                icon::AbstractString=DEFAULT_ICON_LINUX,
                audio_backend::AbstractString=DEFAULT_AUDIO_BACKEND_LINUX)

    if (app_name == "" || length(app_name) < 1)                              # Default for running in REPL
        app_name = "Julia REPL"
    elseif occursin("atom", app_name) && occursin("boot_repl.jl", app_name)  # Default for running in Juno
        app_name = "Atom Juno"
    end
    if sound == true || typeof(sound) <: AbstractString
        if sound == true
            play(backend=audio_backend)
        elseif ispath(sound)
            play(sound=sound, backend=audio_backend)
        end
    end
    run(`notify-send $title $message -u $urgency -a $app_name -i $(icon) -t $(time * 1000)`)

    return
end
notify() = notify("Task completed.")

"""
    Notifications.alarm(;sound="default.wav")

Notify by sound.
If you choose a specific sound WAV file, you can play it instead of the default sound.

# Arguments
- `backend::AbstractString` : a CLI audio program used ("aplay","sox","vlc")
"""
function play(;sound::AbstractString=DEFAULT_SOUND,
                backend::AbstractString=DEFAULT_AUDIO_BACKEND_LINUX)
    if backend == "aplay"
        @async run(`aplay -q $sound`)
    elseif backend == "sox"
        @async run(`play -q $sound`)
    elseif backend == "vlc"
        @async run(`cvlc --play-and-exit --no-loop $sound` |> "/dev/null")
    end
    return nothing
end


"""
    Notifications.alarm(;sound="default.wav")

Notify by sound.
alarm() = play()
"""
alarm() = play()

"""
    Notifications.say(message::AbstractString)

Read a given message aloud.

# Arguments
- `backend::AbstractString` : a CLI test-to-speech program used to synthesize speech ("espreak","festival")
"""
function say(msg::AbstractString;
            backend::AbstractString=DEFAULT_SPEECH_BACKEND_LINUX)
    if backend == "espeak"
        run(pipeline(`espeak $msg`, stderr=devnull))
    elseif backend == "festival"
        run(pipeline(`echo $msg`, `festival --tts`))
    end
end
