DEFAULT_ICON_LINUX = joinpath(@__DIR__, "default.svg");
DEFAULT_ICON_WINDOWS = joinpath(@__DIR__, "default.ico");
DEFAULT_SOUND = joinpath(@__DIR__, "default.wav");

DEFAULT_SPEECH_BACKEND_LINUX = "festival";  # might be espeak or festival
DEFAULT_AUDIO_BACKEND_LINUX = "sox";        # might be alsa, sox or vlc
