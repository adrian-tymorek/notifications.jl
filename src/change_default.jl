"""
    Notifications.set_default(object::AbstractString, new::AbstractString)

A generic function to interact with default configurations.

# Arguments
- `object::AbstractString` : a variable name of a config to change (DEFAULT_SPEECH_BACKEND_LINUX, DEFAULT_AUDIO_BACKEND_LINUX)
- `new::AbstractString` : a new value assigned to a  variable ()
"""
function set_default(object::AbstractString, new::AbstractString)
    fpath = (joinpath("defaults", "defaults.jl"))
    fileIO = open(fpath)
    content = read(fileIO, String)
    close(fileIO)

    if occursin("EMAIL", new)
        old = match(Regex("$object.+=.*?[\"](.+)[\"];"), content)[1]
    else
        old = match(Regex("$object.+=.*?([a-zA-Z]+).*;"), content)[1]
    end
    content = replace(content, "$old" => "$new", count=1)
    println(content)
    println(old)

    fileIO = open(fpath, "w")
    write(fileIO, content)
    close(fileIO)
end

"""
    Notifications.set_speech_backend(backend::AbstractString)

Change speech notification backend.

# Arguments
- `backend::AbstractString` : a name of new backend (festival, espeak)
"""
function set_speech_backend(backend::AbstractString)
    object = DEFAULT_SPEECH_BACKEND_LINUX
    set_default(object, backend)
end

"""
    Notifications.set_audio_backend(backend::AbstractString)

Change a sound notification backend.

# Arguments
- `backend::AbstractString` : a name of new backend (alsa, sox, vlc)
"""
function set_audio_backend(backend::AbstractString)
    object = DEFAULT_AUDIO_BACKEND_LINUX
    set_default(object, backend)
end

object = "DEFAULT_EMAIL_ADDRESS"
new = "mymail@wypchajsie.com"

set_default(object, new)

# fpath = (joinpath("defaults", "defaults.jl"))
# fileIO = open(fpath)
# content = read(fileIO, String)
# close(fileIO)
#
# old = match(Regex("$object.+=.*?([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+).*;"), content)[1]
# println(old)
# content = replace(content, "$old" => "$new", count=1)
# println(content)
