module Notifications

using Pkg, Dates

import Base.notify
export notify, countup, countdown, play, alarm, say
if Sys.islinux()
    export email, register_email
    include("linux.jl")
    include("email.jl")
end
if Sys.isapple()
    export email, register_email
    include("mac.jl")
    include("email.jl")
end
if Sys.iswindows()
    include("windows.jl")
end

include("timer.jl")
end # module
